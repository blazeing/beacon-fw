/**************************************************************************************************
  Filename: Chipolo.h
  Author: Blaz Remskar
  Date: 5.6.2015
  Revision: 0

  Description: Chipolo test application
**************************************************************************************************/

#ifndef CHIPOLODEMO_H
#define CHIPOLODEMO_H

#ifdef __cplusplus
extern "C"
{
#endif

/*********************************************************************
 * INCLUDES
 */

/*********************************************************************
 * CONSTANTS
 */

// Chipolo Task Events
#define CP_START_DEVICE_EVT                              0x0001
#define CP_BATTERY_CHECK_EVT                             0x0002
#define CP_ACCEL_READ_EVT                                0x0004
#define CP_TOGGLE_BUZZER_EVT                             0x0008
#define CP_ADV_IN_CONNECTION_EVT                         0x0010
#define CP_POWERON_LED_TIMEOUT_EVT                       0x0020

/*********************************************************************
 * MACROS
 */

/*********************************************************************
 * FUNCTIONS
 */

/*
 * Task Initialization for the BLE Application
 */
extern void ChipoloApp_Init( uint8 task_id );

/*
 * Task Event Processor for the BLE Application
 */
extern uint16 ChipoloApp_ProcessEvent( uint8 task_id, uint16 events );

/*********************************************************************
*********************************************************************/

#ifdef __cplusplus
}
#endif

#endif /* CHIPOLODEMO_H */
