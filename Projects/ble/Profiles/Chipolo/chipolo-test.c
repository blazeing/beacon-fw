/**************************************************************************************************
  Filename: chipolo-test.c
  Author: Blaz Remskar
  Date: 27.6.2015
  Revision: 0

  Description: Chipolo test service
**************************************************************************************************/

/*********************************************************************
 * INCLUDES
 */
#include "bcomdef.h"
#include "OSAL.h"
#include "linkdb.h"
#include "att.h"
#include "gatt.h"
#include "gatt_uuid.h"
#include "gattservapp.h"

#include "chipolo-test.h"
#include "osal_snv.h"
#include "gap.h"

/*********************************************************************
 * MACROS
 */

/*********************************************************************
 * CONSTANTS
 */

#define SERVAPP_NUM_ATTR_SUPPORTED        19   
   
/*********************************************************************
 * TYPEDEFS
 */

/*********************************************************************
 * GLOBAL VARIABLES
 */
uint8 temp_intervals[4] = {0x00,0x00,0x00,0x00};

// Chipolo Service UUID
CONST uint8 chipoloServUUID[ATT_BT_UUID_SIZE] =
{ 
  LO_UINT16(CHIPOLO_SERVICE_UUID), HI_UINT16(CHIPOLO_SERVICE_UUID)
};

// Chipolo Enabler UUID
CONST uint8 chipoloEnablerUUID[ATT_BT_UUID_SIZE] =
{ 
  LO_UINT16(CHIPOLO_ENABLER_UUID), HI_UINT16(CHIPOLO_ENABLER_UUID)
};

// Chipolo Range UUID
CONST uint8 chipoloRangeUUID[ATT_BT_UUID_SIZE] =
{ 
  LO_UINT16(CHIPOLO_RANGE_UUID), HI_UINT16(CHIPOLO_RANGE_UUID)
};

// Chipolo Data 1 UUID
CONST uint8 chipolo1UUID[ATT_BT_UUID_SIZE] =
{ 
  LO_UINT16(CHIPOLO_1_UUID), HI_UINT16(CHIPOLO_1_UUID)
};

// Chipolo Data 2 UUID
CONST uint8 chipolo2UUID[ATT_BT_UUID_SIZE] =
{ 
  LO_UINT16(CHIPOLO_2_UUID), HI_UINT16(CHIPOLO_2_UUID)
};

// Chipolo Data 3 UUID
CONST uint8 chipolo3UUID[ATT_BT_UUID_SIZE] =
{ 
  LO_UINT16(CHIPOLO_3_UUID), HI_UINT16(CHIPOLO_3_UUID)
};

/*********************************************************************
 * EXTERNAL VARIABLES
 */

/*********************************************************************
 * EXTERNAL FUNCTIONS
 */

/*********************************************************************
 * LOCAL VARIABLES
 */
static chipoloCBs_t *chipolo_AppCBs = NULL;


/*********************************************************************
 * Profile Attributes - variables
 */

// Accelerometer Service attribute
static CONST gattAttrType_t chipoloService = { ATT_BT_UUID_SIZE, chipoloServUUID };


// Enabler Characteristic Properties
static uint8 chipoloEnabledCharProps = GATT_PROP_READ | GATT_PROP_WRITE;

// Enabler Characteristic Value
static uint8 chipoloEnabled = FALSE;

// Enabler Characteristic user description
static uint8 chipoloEnabledUserDesc[16] = "Chipolo Enable\0";


// Range Characteristic Properties
static uint8 chipoloRangeCharProps = GATT_PROP_READ | GATT_PROP_WRITE;

// Range Characteristic Value
static uint32 chipoloRange;

// Range Characteristic user description
static uint8 chipoloRangeUserDesc[15] = "Chipolo Range\0";


// Characteristic Properties
static uint8 chipolo1CharProps = GATT_PROP_NOTIFY;
static uint8 chipolo2CharProps = GATT_PROP_NOTIFY;
static uint8 chipolo3CharProps = GATT_PROP_NOTIFY;

// Characteristics
static int8 chipolo1 = 0;
static int8 chipolo2 = 0;
static int8 chipolo3 = 0;

// Client Characteristic configuration. Each client has its own instantiation
// of the Client Characteristic Configuration. Reads of the Client Characteristic
// Configuration only shows the configuration for that client and writes only
// affect the configuration of that client.

// Accel Coordinate Characteristic Configs
static gattCharCfg_t chipolo1Config[GATT_MAX_NUM_CONN];
static gattCharCfg_t chipolo2Config[GATT_MAX_NUM_CONN];
static gattCharCfg_t chipolo3Config[GATT_MAX_NUM_CONN];

// Accel Coordinate Characteristic user descriptions
static uint8 chipolo1CharUserDesc[10] = "chipolo1\0";
static uint8 chipolo2CharUserDesc[10] = "chipolo2\0";
static uint8 chipolo3CharUserDesc[10] = "chipolo3\0";


/*********************************************************************
 * Profile Attributes - Table
 */
static gattAttribute_t chipoloAttrTbl[SERVAPP_NUM_ATTR_SUPPORTED] = 
{
  // Chipolo Service
  { 
    { ATT_BT_UUID_SIZE, primaryServiceUUID }, /* type */
    GATT_PERMIT_READ,                   /* permissions */
    0,                                  /* handle */
    (uint8 *)&chipoloService                /* pValue */
  },
  
    // Characteristic Declaration
    { 
      { ATT_BT_UUID_SIZE, characterUUID },
      GATT_PERMIT_READ, 
      0,
      &chipoloRangeCharProps 
    },

      // Char Value
      { 
        { ATT_BT_UUID_SIZE, chipoloRangeUUID },
        GATT_PERMIT_READ | GATT_PERMIT_WRITE, 
        0,
        (uint8*)&chipoloRange 
      },

      // User Description
      { 
        { ATT_BT_UUID_SIZE, charUserDescUUID },
        GATT_PERMIT_READ, 
        0,
        chipoloRangeUserDesc 
      },
      
    // Enabler Characteristic Declaration
    { 
      { ATT_BT_UUID_SIZE, characterUUID },
      GATT_PERMIT_READ, 
      0,
      &chipoloEnabledCharProps 
    },

      // Enable Characteristic Value
      { 
        { ATT_BT_UUID_SIZE, chipoloEnablerUUID },
        GATT_PERMIT_READ | GATT_PERMIT_WRITE, 
        0,
        &chipoloEnabled 
      },

      // Enable User Description
      { 
        { ATT_BT_UUID_SIZE, charUserDescUUID },
        GATT_PERMIT_READ, 
        0,
        (uint8*)&chipoloEnabledUserDesc 
      },
       
    // 1 Characteristic Declaration
    { 
      { ATT_BT_UUID_SIZE, characterUUID },
      GATT_PERMIT_READ, 
      0,
      &chipolo1CharProps 
    },
  
      // 1 Characteristic Value
      { 
        { ATT_BT_UUID_SIZE, chipolo1UUID },
        0, 
        0, 
        (uint8 *)&chipolo1
      },
      
      // 1 Characteristic configuration
      { 
        { ATT_BT_UUID_SIZE, clientCharCfgUUID },
        GATT_PERMIT_READ | GATT_PERMIT_WRITE, 
        0, 
        (uint8 *)chipolo1Config 
      },

      // 1User Description
      { 
        { ATT_BT_UUID_SIZE, charUserDescUUID },
        GATT_PERMIT_READ, 
        0, 
        chipolo1CharUserDesc
      },  

   // 2 Characteristic Declaration
    { 
      { ATT_BT_UUID_SIZE, characterUUID },
      GATT_PERMIT_READ, 
      0,
      &chipolo2CharProps 
    },
  
      // 2 Characteristic Value
      { 
        { ATT_BT_UUID_SIZE, chipolo2UUID },
        0, 
        0, 
        (uint8 *)&chipolo2
      },
      
      // 2 Characteristic configuration
      { 
        { ATT_BT_UUID_SIZE, clientCharCfgUUID },
        GATT_PERMIT_READ | GATT_PERMIT_WRITE, 
        0, 
        (uint8 *)chipolo2Config
      },

      // 2 Characteristic User Description
      { 
        { ATT_BT_UUID_SIZE, charUserDescUUID },
        GATT_PERMIT_READ, 
        0, 
        chipolo2CharUserDesc
      },

   // 3 Characteristic Declaration
    { 
      { ATT_BT_UUID_SIZE, characterUUID },
      GATT_PERMIT_READ, 
      0,
      &chipolo3CharProps 
    },
  
      // 3 Characteristic Value
      { 
        { ATT_BT_UUID_SIZE, chipolo3UUID },
        0, 
        0, 
        (uint8 *)&chipolo3
      },
      
      // 3 Characteristic configuration
      { 
        { ATT_BT_UUID_SIZE, clientCharCfgUUID },
        GATT_PERMIT_READ | GATT_PERMIT_WRITE, 
        0, 
        (uint8 *)chipolo3Config
      },

      // 3 Characteristic User Description
      { 
        { ATT_BT_UUID_SIZE, charUserDescUUID },
        GATT_PERMIT_READ, 
        0, 
        chipolo3CharUserDesc
      },  

};


/*********************************************************************
 * LOCAL FUNCTIONS
 */
static uint8 chipolo_ReadAttrCB( uint16 connHandle, gattAttribute_t *pAttr, 
                               uint8 *pValue, uint8 *pLen, uint16 offset, uint8 maxLen );
static bStatus_t chipolo_WriteAttrCB( uint16 connHandle, gattAttribute_t *pAttr,
                                    uint8 *pValue, uint8 len, uint16 offset );

static void chipolo_HandleConnStatusCB( uint16 connHandle, uint8 changeType );

/*********************************************************************
 * PROFILE CALLBACKS
 */
//  Chipolo Service Callbacks
CONST gattServiceCBs_t  chipoloCBs =
{
  chipolo_ReadAttrCB,  // Read callback function pointer
  chipolo_WriteAttrCB, // Write callback function pointer
  NULL               // Authorization callback function pointer
};

/*********************************************************************
 * PUBLIC FUNCTIONS
 */

/*********************************************************************
 * @fn      Chipolo_AddService
 *
 * @brief   Initializes the Accelerometer service by
 *          registering GATT attributes with the GATT server. Only
 *          call this function once.
 *
 * @param   services - services to add. This is a bit map and can
 *                     contain more than one service.
 *
 * @return  Success or Failure
 */
bStatus_t Chipolo_AddService( uint32 services )
{
  uint8 status = SUCCESS;

  // Initialize Client Characteristic Configuration attributes
//  GATTServApp_InitCharCfg( INVALID_CONNHANDLE, chipolo1Config );
//  GATTServApp_InitCharCfg( INVALID_CONNHANDLE, chipolo1Config );
//  GATTServApp_InitCharCfg( INVALID_CONNHANDLE, chipolo1Config );

  // Register with Link DB to receive link status change callback
  VOID linkDB_Register( chipolo_HandleConnStatusCB );  

  if ( services & CHIPOLO_SERVICE )
  {
    // Register GATT attribute list and CBs with GATT Server App
//    status = GATTServApp_RegisterService( chipoloAttrTbl, 
//                                          GATT_NUM_ATTRS( chipoloAttrTbl ),
//                                          &chipoloCBs );
    status = GATTServApp_RegisterService( chipoloAttrTbl, 
                                          3,
                                          &chipoloCBs );
  }

  return ( status );
}

/*********************************************************************
 * @fn      Chipolo_RegisterAppCBs
 *
 * @brief   Does the profile initialization.  Only call this function
 *          once.
 *
 * @param   callbacks - pointer to application callbacks.
 *
 * @return  SUCCESS or bleAlreadyInRequestedMode
 */
bStatus_t Chipolo_RegisterAppCBs( chipoloCBs_t *appCallbacks )
{
  if ( appCallbacks )
  {
    chipolo_AppCBs = appCallbacks;
    
    return ( SUCCESS );
  }
  else
  {
    return ( bleAlreadyInRequestedMode );
  }
}


/*********************************************************************
 * @fn      Chipolo_SetParameter
 *
 * @brief   Set Profile parameter.
 *
 * @param   param - Profile parameter ID
 * @param   len - length of data to right
 * @param   value - pointer to data to write.  This is dependent on
 *          the parameter ID and WILL be cast to the appropriate 
 *          data type (example: data type of uint16 will be cast to 
 *          uint16 pointer).
 *
 * @return  bStatus_t
 */
bStatus_t Chipolo_SetParameter( uint8 param, uint8 len, void *value )
{
  bStatus_t ret = SUCCESS;

  switch ( param )
  {
    case CHIPOLO_ENABLER:
      if ( len == sizeof ( uint8 ) ) 
      {
        chipoloEnabled = *((uint8*)value);
      }
      else
      {
        ret = bleInvalidRange;
      }
      break;
      
    case CHIPOLO_RANGE:
      if ( len == sizeof ( uint32 )) 
      {
        chipoloRange = *((uint32*)value);
      }
      else
      {
        ret = bleInvalidRange;
      }
      break;
      
    case CHIPOLO_1_ATTR:
      if ( len == sizeof ( int8 ) ) 
      {      
        chipolo1 = *((int8*)value);

        // See if Notification has been enabled
        GATTServApp_ProcessCharCfg( chipolo1Config, (uint8 *)&chipolo1,
                                    FALSE, chipoloAttrTbl, GATT_NUM_ATTRS( chipoloAttrTbl ),
                                    INVALID_TASK_ID );
      }
      else
      {
        ret = bleInvalidRange;
      }
      break;

    case CHIPOLO_2_ATTR:
      if ( len == sizeof ( int8 ) ) 
      {      
        chipolo2 = *((int8*)value);

        // See if Notification has been enabled
        GATTServApp_ProcessCharCfg( chipolo2Config, (uint8 *)&chipolo2,
                                    FALSE, chipoloAttrTbl, GATT_NUM_ATTRS( chipoloAttrTbl ),
                                    INVALID_TASK_ID );
      }
      else
      {
        ret = bleInvalidRange;
      }
      break;

    case CHIPOLO_3_ATTR:
      if ( len == sizeof ( int8 ) ) 
      {      
        chipolo3 = *((int8*)value);

        // See if Notification has been enabled
        GATTServApp_ProcessCharCfg( chipolo3Config, (uint8 *)&chipolo3,
                                    FALSE, chipoloAttrTbl, GATT_NUM_ATTRS( chipoloAttrTbl ),
                                    INVALID_TASK_ID );
      }
      else
      {
        ret = bleInvalidRange;
      }
      break;
      
    default:
      ret = INVALIDPARAMETER;
      break;
  }
  
  return ( ret );
}

/*********************************************************************
 * @fn      Chipolo_GetParameter
 *
 * @brief   Get Profile parameter.
 *
 * @param   param - Profile parameter ID
 * @param   value - pointer to data to put.  This is dependent on
 *          the parameter ID and WILL be cast to the appropriate 
 *          data type (example: data type of uint16 will be cast to 
 *          uint16 pointer).
 *
 * @return  bStatus_t
 */
bStatus_t Chipolo_GetParameter( uint8 param, void *value )
{
  bStatus_t ret = SUCCESS;
  switch ( param )
  {
    case CHIPOLO_ENABLER:
      *((uint8*)value) = chipoloEnabled;
      break;
      
    case CHIPOLO_RANGE:
      *((uint32*)value) = chipoloRange;
      break;
      
    case CHIPOLO_1_ATTR:
      *((int8*)value) = chipolo1;
      break;

    case CHIPOLO_2_ATTR:
      *((int8*)value) = chipolo2;
      break;

    case CHIPOLO_3_ATTR:
      *((int8*)value) = chipolo3;
      break;
      
    default:
      ret = INVALIDPARAMETER;
      break;
  }
  
  return ( ret );
}

/*********************************************************************
 * @fn          chipolo_ReadAttr
 *
 * @brief       Read an attribute.
 *
 * @param       pAttr - pointer to attribute
 * @param       pLen - length of data to be read
 * @param       pValue - pointer to data to be read
 * @param       signature - whether to include Authentication Signature
 *
 * @return      Success or Failure
 */
static uint8 chipolo_ReadAttrCB( uint16 connHandle, gattAttribute_t *pAttr, 
                               uint8 *pValue, uint8 *pLen, uint16 offset, uint8 maxLen )
{
  uint16 uuid;
  bStatus_t status = SUCCESS;
  uint8 intervals[4] = {0x00,0x00,0x00,0x00};

  // Make sure it's not a blob operation
  if ( offset > 0 )
  {
    return ( ATT_ERR_ATTR_NOT_LONG );
  }

  if ( pAttr->type.len == ATT_BT_UUID_SIZE )
  {    
    // 16-bit UUID
    uuid = BUILD_UINT16( pAttr->type.uuid[0], pAttr->type.uuid[1]);
    switch ( uuid )
    {
      // No need for "GATT_SERVICE_UUID" or "GATT_CLIENT_CHAR_CFG_UUID" cases;
      // gattserverapp handles those types for reads
      case CHIPOLO_RANGE_UUID:
        osal_snv_read(BLE_INTERVALS, 4, intervals);
        *pLen = 4;
        pValue[0] = intervals[0]; //LO_UINT32( *((uint32 *)pAttr->pValue) );
        pValue[1] = intervals[1]; //HI_UINT16( *((uint16 *)pAttr->pValue) );
        pValue[2] = intervals[2]; //LO_UINT16( *((uint16 *)pAttr->pValue) );
        pValue[3] = intervals[3]; //HI_UINT16( *((uint16 *)pAttr->pValue) );
        break;
  
      case CHIPOLO_ENABLER_UUID:
      case CHIPOLO_1_UUID:
      case CHIPOLO_2_UUID:
      case CHIPOLO_3_UUID:
        *pLen = 1;
        pValue[0] = *pAttr->pValue;
        break;
      
      default:
        // Should never get here!
        *pLen = 0;
        status = ATT_ERR_ATTR_NOT_FOUND;
        break;
    }
  }
  else
  {
    // 128-bit UUID
    *pLen = 0;
    status = ATT_ERR_INVALID_HANDLE;
  }


  return ( status );
}

/*********************************************************************
 * @fn      chipolo_WriteAttrCB
 *
 * @brief   Validate attribute data prior to a write operation
 *
 * @param   connHandle � connection message was received on
 * @param   pReq - pointer to request
 *
 * @return  Success or Failure
 */
static bStatus_t chipolo_WriteAttrCB( uint16 connHandle, gattAttribute_t *pAttr,
                                    uint8 *pValue, uint8 len, uint16 offset )
{
  bStatus_t status = SUCCESS;
  uint8 notify = 0xFF;
  uint16 ADVERTISING_INTERVAL;
  uint16 CONN_INTERVAL;

  if ( pAttr->type.len == ATT_BT_UUID_SIZE )
  {
    
    uint16 uuid = BUILD_UINT16( pAttr->type.uuid[0], pAttr->type.uuid[1]);
    switch ( uuid )
    {
      case CHIPOLO_ENABLER_UUID:
        //Validate the value
        // Make sure it's not a blob oper
        if ( offset == 0 )
        {
          if ( len > 1 )
            status = ATT_ERR_INVALID_VALUE_SIZE;
          else if ( pValue[0] != FALSE && pValue[0] != TRUE )
            status = ATT_ERR_INVALID_VALUE;
        }
        else
        {
          status = ATT_ERR_ATTR_NOT_LONG;
        }
        
        //Write the value
        if ( status == SUCCESS )
        {
          uint8 *pCurValue = (uint8 *)pAttr->pValue;
          
          *pCurValue = pValue[0];
          notify = CHIPOLO_ENABLER;        
        }
             
        break;
        
      case CHIPOLO_RANGE_UUID:
        //Validate the value
        // Make sure it's not a blob oper
        if ( offset == 0 )
        {
          if ( len > 4 )
            status = ATT_ERR_INVALID_VALUE_SIZE;
        }
        else
        {
          status = ATT_ERR_ATTR_NOT_LONG;
        }
        
        //Write the value
        if ( status == SUCCESS )
        {

          temp_intervals[0] = pValue[0];
          temp_intervals[1] = pValue[1];
          temp_intervals[2] = pValue[2];
          temp_intervals[3] = pValue[3];
          
          
          ADVERTISING_INTERVAL = (uint16)((temp_intervals[0] << 8) | temp_intervals[1]);
          CONN_INTERVAL = (uint16)((temp_intervals[2] << 8) | temp_intervals[3]);
          
          // The advertising interval is from the 20ms to 10.24s
          // The connection interval for iOS is from the 8ms to 1.6s
          if((ADVERTISING_INTERVAL < 10240) && (ADVERTISING_INTERVAL > 20) && (CONN_INTERVAL < 1600) && (CONN_INTERVAL > 8))
          {
            notify = 0x00;
            osal_snv_write(BLE_INTERVALS, 4, temp_intervals);
          }
        }  
        
        break;
          
      case GATT_CLIENT_CHAR_CFG_UUID:
        status = GATTServApp_ProcessCCCWriteReq( connHandle, pAttr, pValue, len,
                                                 offset, GATT_CLIENT_CFG_NOTIFY );
        break;      
          
      default:
          // Should never get here!
          status = ATT_ERR_ATTR_NOT_FOUND;
    }
  }
  else
  {
    // 128-bit UUID
    status = ATT_ERR_INVALID_HANDLE;
  }  

  // If an attribute changed then callback function to notify application of change
  if ( (notify != 0xFF) && chipolo_AppCBs && chipolo_AppCBs->pfnChipoloEnabler )
    chipolo_AppCBs->pfnChipoloEnabler();  
  
//  if ( (notify != 0xFF) && chipolo_AppCBs && chipolo_AppCBs->pfnChipoloRange )
//    chipolo_AppCBs->pfnChipoloRange();  
  
  return ( status );
}

/*********************************************************************
 * @fn          chipolo_HandleConnStatusCB
 *
 * @brief       ervice link status change handler function.
 *
 * @param       connHandle - connection handle
 * @param       changeType - type of change
 *
 * @return      none
 */
static void chipolo_HandleConnStatusCB( uint16 connHandle, uint8 changeType )
{ 
  // Make sure this is not loopback connection
  if ( connHandle != LOOPBACK_CONNHANDLE )
  {
    // Reset Client Char Config if connection has dropped
    if ( ( changeType == LINKDB_STATUS_UPDATE_REMOVED )      ||
         ( ( changeType == LINKDB_STATUS_UPDATE_STATEFLAGS ) && 
           ( !linkDB_Up( connHandle ) ) ) )
    { 
      GATTServApp_InitCharCfg( connHandle, chipolo1Config );
      GATTServApp_InitCharCfg( connHandle, chipolo2Config );
      GATTServApp_InitCharCfg( connHandle, chipolo3Config );
    }
  }
}

void setChipolo1Parameter(void)
{
  static uint8 num = GATT_NUM_ATTRS( chipoloAttrTbl );
  Chipolo_SetParameter(CHIPOLO_1_ATTR, sizeof ( int8 ), &num);
}

/*********************************************************************
*********************************************************************/
