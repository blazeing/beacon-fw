/**************************************************************************************************
  Filename: cBeacon.h
  Author: Blaz Remskar
  Date: 5.14.2015
  Revision: 0

  Description: This file contains the cBeacon BLE peripheral application definitions and prototypes.

**************************************************************************************************/

#ifndef CBEACON_H
#define CBEACON_H

#ifdef __cplusplus
extern "C"
{
#endif

/*********************************************************************
 * INCLUDES
 */

/*********************************************************************
 * CONSTANTS
 */


// cBeacon BLE Peripheral Task Events
#define CBE_START_DEVICE_EVT                              0x0001
#define CBE_PERIODIC_EVT                                  0x0002
#define CBE_TIMEOUT_EVT                                   0x0004   

/*********************************************************************
 * MACROS
 */
   
#define ASSERT_CS()                                                            \
{                                                                              \
  P1 &= ~(1 << 2);                                                             \
}

#define RELEASE_CS()                                                           \
{                                                                              \
  P1 |= (1 << 2);                                                              \
}

/*********************************************************************
 * FUNCTIONS
 */

/*
 * Task Initialization for the BLE Application
 */
extern void BLEcBeacon_Init( uint8 task_id );

/*
 * Task Event Processor for the BLE Application
 */
extern uint16 BLEcBeacon_ProcessEvent( uint8 task_id, uint16 events );

/*********************************************************************
*********************************************************************/

#ifdef __cplusplus
}
#endif

#endif /* CBEACON_H */
