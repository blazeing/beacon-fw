/**************************************************************************************************
  Filename: cbeaconGATTprofile.h
  Author: Blaz Remskar
  Date: 5.14.2015
  Revision: 0

  Description: This file contains the cBeacon GATT profile definitions and prototypes.

**************************************************************************************************/

#ifndef CBEACONGATTPROFILE_H
#define CBEACONGATTPROFILE_H

#ifdef __cplusplus
extern "C"
{
#endif

/*********************************************************************
 * INCLUDES
 */

/*********************************************************************
 * CONSTANTS
 */

// Profile Parameters
#define CBEACONPROFILE_CHAR1                   0  // RW uint8 - Profile Characteristic 1 value 
#define CBEACONPROFILE_CHAR2                   1  // RW uint8 - Profile Characteristic 2 value
#define CBEACONPROFILE_CHAR3                   2  // RW uint8 - Profile Characteristic 3 value
#define CBEACONPROFILE_CHAR4                   3  // RW uint8 - Profile Characteristic 4 value
#define CBEACONPROFILE_CHAR5                   4  // RW uint8 - Profile Characteristic 4 value
  
// cBeacon Profile Service UUID
#define CBEACONPROFILE_SERV_UUID               0xFFF0
    
// Key Pressed UUID
#define CBEACONPROFILE_CHAR1_UUID            0xFFF1
#define CBEACONPROFILE_CHAR2_UUID            0xFFF2
#define CBEACONPROFILE_CHAR3_UUID            0xFFF3
#define CBEACONPROFILE_CHAR4_UUID            0xFFF4
#define CBEACONPROFILE_CHAR5_UUID            0xFFF5
  
// cBeacon Keys Profile Services bit fields
#define CBEACONPROFILE_SERVICE               0x00000001

// Length of Characteristic 5 in bytes
#define CBEACONPROFILE_CHAR5_LEN           5  

/*********************************************************************
 * TYPEDEFS
 */

  
/*********************************************************************
 * MACROS
 */

/*********************************************************************
 * Profile Callbacks
 */

// Callback when a characteristic value has changed
typedef void (*cbeaconProfileChange_t)( uint8 paramID );

typedef struct
{
  cbeaconProfileChange_t        pfncBeaconProfileChange;  // Called when characteristic value changes
} cbeaconProfileCBs_t;

    

/*********************************************************************
 * API FUNCTIONS 
 */


/*
 * cBeaconProfile_AddService- Initializes the cBeacon GATT Profile service by registering
 *          GATT attributes with the GATT server.
 *
 * @param   services - services to add. This is a bit map and can
 *                     contain more than one service.
 */

extern bStatus_t cBeaconProfile_AddService( uint32 services );

/*
 * cBeaconProfile_RegisterAppCBs - Registers the application callback function.
 *                    Only call this function once.
 *
 *    appCallbacks - pointer to application callbacks.
 */
extern bStatus_t cBeaconProfile_RegisterAppCBs( cbeaconProfileCBs_t *appCallbacks );

/*
 * cBeaconProfile_SetParameter - Set a cBeacon GATT Profile parameter.
 *
 *    param - Profile parameter ID
 *    len - length of data to right
 *    value - pointer to data to write.  This is dependent on
 *          the parameter ID and WILL be cast to the appropriate 
 *          data type (example: data type of uint16 will be cast to 
 *          uint16 pointer).
 */
extern bStatus_t cBeaconProfile_SetParameter( uint8 param, uint8 len, void *value );
  
/*
 * cBeaconProfile_GetParameter - Get a cBeacon GATT Profile parameter.
 *
 *    param - Profile parameter ID
 *    value - pointer to data to write.  This is dependent on
 *          the parameter ID and WILL be cast to the appropriate 
 *          data type (example: data type of uint16 will be cast to 
 *          uint16 pointer).
 */
extern bStatus_t cBeaconProfile_GetParameter( uint8 param, void *value );


/*********************************************************************
*********************************************************************/

#ifdef __cplusplus
}
#endif

#endif /* CBEACONGATTPROFILE_H */
