/**************************************************************************************************
  Filename: iBeaconChipolo.h
  Author: Blaz Remskar
  Date: 5.14.2015
  Revision: 0

  Description: This file contains the iBeacon Chipolo application.

**************************************************************************************************/

#ifndef IBEACONCHIPOLO_H
#define IBEACONCHIPOLO_H

#ifdef __cplusplus
extern "C"
{
#endif

/*********************************************************************
 * INCLUDES
 */

/*********************************************************************
 * CONSTANTS
 */

// iBeacon Chipolo Task Events
#define iBC_START_DEVICE_EVT                              0x0001
#define iBC_PERIODIC_EVT                                  0x0002
#define iBC_ADV_IN_CONNECTION_EVT                         0x0004

/*********************************************************************
 * MACROS
 */

/*********************************************************************
 * FUNCTIONS
 */

/*
 * Task Initialization for the BLE iBeaconChipolo Application
 */
extern void iBeaconChipoloBLE_Init( uint8 task_id );

/*
 * Task Event Processor for the BLE iBeaconChipolo Application
 */
extern uint16 iBeaconChipoloBLE_ProcessEvent( uint8 task_id, uint16 events );

/*********************************************************************
*********************************************************************/

#ifdef __cplusplus
}
#endif

#endif /* IBEACONCHIPOLO_H */
