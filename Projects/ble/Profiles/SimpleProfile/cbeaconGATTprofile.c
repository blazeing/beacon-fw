/**************************************************************************************************
  Filename: cbeaconGATTprofile.c
  Author: Blaz Remskar
  Date: 5.14.2015
  Revision: 0

  Description: This file contains the cBeacon GATT profile service.
 
**************************************************************************************************/

/*********************************************************************
 * INCLUDES
 */
#include "bcomdef.h"
#include "OSAL.h"
#include "linkdb.h"
#include "att.h"
#include "gatt.h"
#include "gatt_uuid.h"
#include "gattservapp.h"
#include "gapbondmgr.h"

#include "cbeaconGATTprofile.h"

/*********************************************************************
 * MACROS
 */

/*********************************************************************
 * CONSTANTS
 */

#define SERVAPP_NUM_ATTR_SUPPORTED        17

/*********************************************************************
 * TYPEDEFS
 */

/*********************************************************************
 * GLOBAL VARIABLES
 */
// cBeacon GATT Profile Service UUID: 0xFFF0
CONST uint8 cbeaconProfileServUUID[ATT_BT_UUID_SIZE] =
{ 
  LO_UINT16(CBEACONPROFILE_SERV_UUID), HI_UINT16(CBEACONPROFILE_SERV_UUID)
};

// Characteristic 1 UUID: 0xFFF1
CONST uint8 cbeaconProfilechar1UUID[ATT_BT_UUID_SIZE] =
{ 
  LO_UINT16(CBEACONPROFILE_CHAR1_UUID), HI_UINT16(CBEACONPROFILE_CHAR1_UUID)
};

// Characteristic 2 UUID: 0xFFF2
CONST uint8 cbeaconProfilechar2UUID[ATT_BT_UUID_SIZE] =
{ 
  LO_UINT16(CBEACONPROFILE_CHAR2_UUID), HI_UINT16(CBEACONPROFILE_CHAR2_UUID)
};

// Characteristic 3 UUID: 0xFFF3
CONST uint8 cbeaconProfilechar3UUID[ATT_BT_UUID_SIZE] =
{ 
  LO_UINT16(CBEACONPROFILE_CHAR3_UUID), HI_UINT16(CBEACONPROFILE_CHAR3_UUID)
};

// Characteristic 4 UUID: 0xFFF4
CONST uint8 cbeaconProfilechar4UUID[ATT_BT_UUID_SIZE] =
{ 
  LO_UINT16(CBEACONPROFILE_CHAR4_UUID), HI_UINT16(CBEACONPROFILE_CHAR4_UUID)
};

// Characteristic 5 UUID: 0xFFF5
CONST uint8 cbeaconProfilechar5UUID[ATT_BT_UUID_SIZE] =
{ 
  LO_UINT16(CBEACONPROFILE_CHAR5_UUID), HI_UINT16(CBEACONPROFILE_CHAR5_UUID)
};

/*********************************************************************
 * EXTERNAL VARIABLES
 */

/*********************************************************************
 * EXTERNAL FUNCTIONS
 */

/*********************************************************************
 * LOCAL VARIABLES
 */

static cbeaconProfileCBs_t *cbeaconProfile_AppCBs = NULL;

/*********************************************************************
 * Profile Attributes - variables
 */

// cBeacon Profile Service attribute
static CONST gattAttrType_t cbeaconProfileService = { ATT_BT_UUID_SIZE, cbeaconProfileServUUID };


// cBeacon Profile Characteristic 1 Properties
static uint8 cbeaconProfileChar1Props = GATT_PROP_READ | GATT_PROP_WRITE;

// Characteristic 1 Value
static uint8 cbeaconProfileChar1 = 0;

// cBeacon Profile Characteristic 1 User Description
static uint8 cbeaconProfileChar1UserDesp[17] = "Characteristic 1\0";


// cBeacon Profile Characteristic 2 Properties
static uint8 cbeaconProfileChar2Props = GATT_PROP_READ;

// Characteristic 2 Value
static uint8 cbeaconProfileChar2 = 0;

// cBeacon Profile Characteristic 2 User Description
static uint8 cbeaconProfileChar2UserDesp[17] = "Characteristic 2\0";


// cBeacon Profile Characteristic 3 Properties
static uint8 cbeaconProfileChar3Props = GATT_PROP_WRITE;

// Characteristic 3 Value
static uint8 cbeaconProfileChar3 = 0;

// cBeacon Profile Characteristic 3 User Description
static uint8 cbeaconProfileChar3UserDesp[17] = "Characteristic 3\0";


// cBeacon Profile Characteristic 4 Properties
static uint8 cbeaconProfileChar4Props = GATT_PROP_NOTIFY;

// Characteristic 4 Value
static uint8 cbeaconProfileChar4 = 0;

// cBeacon Profile Characteristic 4 Configuration Each client has its own
// instantiation of the Client Characteristic Configuration. Reads of the
// Client Characteristic Configuration only shows the configuration for
// that client and writes only affect the configuration of that client.
static gattCharCfg_t cbeaconProfileChar4Config[GATT_MAX_NUM_CONN];
                                        
// cBeacon Profile Characteristic 4 User Description
static uint8 cbeaconProfileChar4UserDesp[17] = "Characteristic 4\0";


// cBeacon Profile Characteristic 5 Properties
static uint8 cbeaconProfileChar5Props = GATT_PROP_READ;

// Characteristic 5 Value
static uint8 cbeaconProfileChar5[CBEACONPROFILE_CHAR5_LEN] = { 0, 0, 0, 0, 0 };

// cBeacon Profile Characteristic 5 User Description
static uint8 cbeaconProfileChar5UserDesp[17] = "Characteristic 5\0";


/*********************************************************************
 * Profile Attributes - Table
 */

static gattAttribute_t cbeaconProfileAttrTbl[SERVAPP_NUM_ATTR_SUPPORTED] = 
{
  // cBeacon Profile Service
  { 
    { ATT_BT_UUID_SIZE, primaryServiceUUID }, /* type */
    GATT_PERMIT_READ,                         /* permissions */
    0,                                        /* handle */
    (uint8 *)&cbeaconProfileService            /* pValue */
  },

    // Characteristic 1 Declaration
    { 
      { ATT_BT_UUID_SIZE, characterUUID },
      GATT_PERMIT_READ, 
      0,
      &cbeaconProfileChar1Props 
    },

      // Characteristic Value 1
      { 
        { ATT_BT_UUID_SIZE, cbeaconProfilechar1UUID },
        GATT_PERMIT_READ | GATT_PERMIT_WRITE, 
        0, 
        &cbeaconProfileChar1 
      },

      // Characteristic 1 User Description
      { 
        { ATT_BT_UUID_SIZE, charUserDescUUID },
        GATT_PERMIT_READ, 
        0, 
        cbeaconProfileChar1UserDesp 
      },      

    // Characteristic 2 Declaration
    { 
      { ATT_BT_UUID_SIZE, characterUUID },
      GATT_PERMIT_READ, 
      0,
      &cbeaconProfileChar2Props 
    },

      // Characteristic Value 2
      { 
        { ATT_BT_UUID_SIZE, cbeaconProfilechar2UUID },
        GATT_PERMIT_READ, 
        0, 
        &cbeaconProfileChar2 
      },

      // Characteristic 2 User Description
      { 
        { ATT_BT_UUID_SIZE, charUserDescUUID },
        GATT_PERMIT_READ, 
        0, 
        cbeaconProfileChar2UserDesp 
      },           
      
    // Characteristic 3 Declaration
    { 
      { ATT_BT_UUID_SIZE, characterUUID },
      GATT_PERMIT_READ, 
      0,
      &cbeaconProfileChar3Props 
    },

      // Characteristic Value 3
      { 
        { ATT_BT_UUID_SIZE, cbeaconProfilechar3UUID },
        GATT_PERMIT_WRITE, 
        0, 
        &cbeaconProfileChar3 
      },

      // Characteristic 3 User Description
      { 
        { ATT_BT_UUID_SIZE, charUserDescUUID },
        GATT_PERMIT_READ, 
        0, 
        cbeaconProfileChar3UserDesp 
      },

    // Characteristic 4 Declaration
    { 
      { ATT_BT_UUID_SIZE, characterUUID },
      GATT_PERMIT_READ, 
      0,
      &cbeaconProfileChar4Props 
    },

      // Characteristic Value 4
      { 
        { ATT_BT_UUID_SIZE, cbeaconProfilechar4UUID },
        0, 
        0, 
        &cbeaconProfileChar4 
      },

      // Characteristic 4 configuration
      { 
        { ATT_BT_UUID_SIZE, clientCharCfgUUID },
        GATT_PERMIT_READ | GATT_PERMIT_WRITE, 
        0, 
        (uint8 *)cbeaconProfileChar4Config 
      },
      
      // Characteristic 4 User Description
      { 
        { ATT_BT_UUID_SIZE, charUserDescUUID },
        GATT_PERMIT_READ, 
        0, 
        cbeaconProfileChar4UserDesp 
      },
      
    // Characteristic 5 Declaration
    { 
      { ATT_BT_UUID_SIZE, characterUUID },
      GATT_PERMIT_READ, 
      0,
      &cbeaconProfileChar5Props 
    },

      // Characteristic Value 5
      { 
        { ATT_BT_UUID_SIZE, cbeaconProfilechar5UUID },
        GATT_PERMIT_AUTHEN_READ, 
        0, 
        cbeaconProfileChar5 
      },

      // Characteristic 5 User Description
      { 
        { ATT_BT_UUID_SIZE, charUserDescUUID },
        GATT_PERMIT_READ, 
        0, 
        cbeaconProfileChar5UserDesp 
      },


};


/*********************************************************************
 * LOCAL FUNCTIONS
 */
static uint8 cbeaconProfile_ReadAttrCB( uint16 connHandle, gattAttribute_t *pAttr, 
                            uint8 *pValue, uint8 *pLen, uint16 offset, uint8 maxLen );
static bStatus_t cbeaconProfile_WriteAttrCB( uint16 connHandle, gattAttribute_t *pAttr,
                                 uint8 *pValue, uint8 len, uint16 offset );

static void cbeaconProfile_HandleConnStatusCB( uint16 connHandle, uint8 changeType );


/*********************************************************************
 * PROFILE CALLBACKS
 */
// cBeacon Profile Service Callbacks
CONST gattServiceCBs_t cbeaconProfileCBs =
{
  cbeaconProfile_ReadAttrCB,  // Read callback function pointer
  cbeaconProfile_WriteAttrCB, // Write callback function pointer
  NULL                       // Authorization callback function pointer
};

/*********************************************************************
 * PUBLIC FUNCTIONS
 */

/*********************************************************************
 * @fn      cBeaconProfile_AddService
 *
 * @brief   Initializes the cBeacon Profile service by registering
 *          GATT attributes with the GATT server.
 *
 * @param   services - services to add. This is a bit map and can
 *                     contain more than one service.
 *
 * @return  Success or Failure
 */
bStatus_t cBeaconProfile_AddService( uint32 services )
{
  uint8 status = SUCCESS;

  // Initialize Client Characteristic Configuration attributes
  GATTServApp_InitCharCfg( INVALID_CONNHANDLE, cbeaconProfileChar4Config );

  // Register with Link DB to receive link status change callback
  VOID linkDB_Register( cbeaconProfile_HandleConnStatusCB );  
  
  if ( services & CBEACONPROFILE_SERVICE )
  {
    // Register GATT attribute list and CBs with GATT Server App
    status = GATTServApp_RegisterService( cbeaconProfileAttrTbl, 
                                          GATT_NUM_ATTRS( cbeaconProfileAttrTbl ),
                                          &cbeaconProfileCBs );
  }

  return ( status );
}


/*********************************************************************
 * @fn      cBeaconProfile_RegisterAppCBs
 *
 * @brief   Registers the application callback function. Only call 
 *          this function once.
 *
 * @param   callbacks - pointer to application callbacks.
 *
 * @return  SUCCESS or bleAlreadyInRequestedMode
 */
bStatus_t cBeaconProfile_RegisterAppCBs( cbeaconProfileCBs_t *appCallbacks )
{
  if ( appCallbacks )
  {
    cbeaconProfile_AppCBs = appCallbacks;
    
    return ( SUCCESS );
  }
  else
  {
    return ( bleAlreadyInRequestedMode );
  }
}
  

/*********************************************************************
 * @fn      cBeaconProfile_SetParameter
 *
 * @brief   Set a cBeacon Profile parameter.
 *
 * @param   param - Profile parameter ID
 * @param   len - length of data to right
 * @param   value - pointer to data to write.  This is dependent on
 *          the parameter ID and WILL be cast to the appropriate 
 *          data type (example: data type of uint16 will be cast to 
 *          uint16 pointer).
 *
 * @return  bStatus_t
 */
bStatus_t cBeaconProfile_SetParameter( uint8 param, uint8 len, void *value )
{
  bStatus_t ret = SUCCESS;
  switch ( param )
  {
    case CBEACONPROFILE_CHAR1:
      if ( len == sizeof ( uint8 ) ) 
      {
        cbeaconProfileChar1 = *((uint8*)value);
      }
      else
      {
        ret = bleInvalidRange;
      }
      break;

    case CBEACONPROFILE_CHAR2:
      if ( len == sizeof ( uint8 ) ) 
      {
        cbeaconProfileChar2 = *((uint8*)value);
      }
      else
      {
        ret = bleInvalidRange;
      }
      break;

    case CBEACONPROFILE_CHAR3:
      if ( len == sizeof ( uint8 ) ) 
      {
        cbeaconProfileChar3 = *((uint8*)value);
      }
      else
      {
        ret = bleInvalidRange;
      }
      break;

    case CBEACONPROFILE_CHAR4:
      if ( len == sizeof ( uint8 ) ) 
      {
        cbeaconProfileChar4 = *((uint8*)value);
        
        // See if Notification has been enabled
        GATTServApp_ProcessCharCfg( cbeaconProfileChar4Config, &cbeaconProfileChar4, FALSE,
                                    cbeaconProfileAttrTbl, GATT_NUM_ATTRS( cbeaconProfileAttrTbl ),
                                    INVALID_TASK_ID );
      }
      else
      {
        ret = bleInvalidRange;
      }
      break;

    case CBEACONPROFILE_CHAR5:
      if ( len == CBEACONPROFILE_CHAR5_LEN ) 
      {
        VOID osal_memcpy( cbeaconProfileChar5, value, CBEACONPROFILE_CHAR5_LEN );
      }
      else
      {
        ret = bleInvalidRange;
      }
      break;
      
    default:
      ret = INVALIDPARAMETER;
      break;
  }
  
  return ( ret );
}

/*********************************************************************
 * @fn      cBeaconProfile_GetParameter
 *
 * @brief   Get a cBeacon Profile parameter.
 *
 * @param   param - Profile parameter ID
 * @param   value - pointer to data to put.  This is dependent on
 *          the parameter ID and WILL be cast to the appropriate 
 *          data type (example: data type of uint16 will be cast to 
 *          uint16 pointer).
 *
 * @return  bStatus_t
 */
bStatus_t cBeaconProfile_GetParameter( uint8 param, void *value )
{
  bStatus_t ret = SUCCESS;
  switch ( param )
  {
    case CBEACONPROFILE_CHAR1:
      *((uint8*)value) = cbeaconProfileChar1;
      break;

    case CBEACONPROFILE_CHAR2:
      *((uint8*)value) = cbeaconProfileChar2;
      break;      

    case CBEACONPROFILE_CHAR3:
      *((uint8*)value) = cbeaconProfileChar3;
      break;  

    case CBEACONPROFILE_CHAR4:
      *((uint8*)value) = cbeaconProfileChar4;
      break;

    case CBEACONPROFILE_CHAR5:
      VOID osal_memcpy( value, cbeaconProfileChar5, CBEACONPROFILE_CHAR5_LEN );
      break;      
      
    default:
      ret = INVALIDPARAMETER;
      break;
  }
  
  return ( ret );
}

/*********************************************************************
 * @fn          cbeaconProfile_ReadAttrCB
 *
 * @brief       Read an attribute.
 *
 * @param       connHandle - connection message was received on
 * @param       pAttr - pointer to attribute
 * @param       pValue - pointer to data to be read
 * @param       pLen - length of data to be read
 * @param       offset - offset of the first octet to be read
 * @param       maxLen - maximum length of data to be read
 *
 * @return      Success or Failure
 */
static uint8 cbeaconProfile_ReadAttrCB( uint16 connHandle, gattAttribute_t *pAttr, 
                            uint8 *pValue, uint8 *pLen, uint16 offset, uint8 maxLen )
{
  bStatus_t status = SUCCESS;

  // If attribute permissions require authorization to read, return error
  if ( gattPermitAuthorRead( pAttr->permissions ) )
  {
    // Insufficient authorization
    return ( ATT_ERR_INSUFFICIENT_AUTHOR );
  }
  
  // Make sure it's not a blob operation (no attributes in the profile are long)
  if ( offset > 0 )
  {
    return ( ATT_ERR_ATTR_NOT_LONG );
  }
 
  if ( pAttr->type.len == ATT_BT_UUID_SIZE )
  {
    // 16-bit UUID
    uint16 uuid = BUILD_UINT16( pAttr->type.uuid[0], pAttr->type.uuid[1]);
    switch ( uuid )
    {
      // No need for "GATT_SERVICE_UUID" or "GATT_CLIENT_CHAR_CFG_UUID" cases;
      // gattserverapp handles those reads

      // characteristics 1 and 2 have read permissions
      // characteritisc 3 does not have read permissions; therefore it is not
      //   included here
      // characteristic 4 does not have read permissions, but because it
      //   can be sent as a notification, it is included here
      case CBEACONPROFILE_CHAR1_UUID:
      case CBEACONPROFILE_CHAR2_UUID:
      case CBEACONPROFILE_CHAR4_UUID:
        *pLen = 1;
        pValue[0] = *pAttr->pValue;
        break;

      case CBEACONPROFILE_CHAR5_UUID:
        *pLen = CBEACONPROFILE_CHAR5_LEN;
        VOID osal_memcpy( pValue, pAttr->pValue, CBEACONPROFILE_CHAR5_LEN );
        break;
        
      default:
        // Should never get here! (characteristics 3 and 4 do not have read permissions)
        *pLen = 0;
        status = ATT_ERR_ATTR_NOT_FOUND;
        break;
    }
  }
  else
  {
    // 128-bit UUID
    *pLen = 0;
    status = ATT_ERR_INVALID_HANDLE;
  }

  return ( status );
}

/*********************************************************************
 * @fn      cbeaconProfile_WriteAttrCB
 *
 * @brief   Validate attribute data prior to a write operation
 *
 * @param   connHandle - connection message was received on
 * @param   pAttr - pointer to attribute
 * @param   pValue - pointer to data to be written
 * @param   len - length of data
 * @param   offset - offset of the first octet to be written
 *
 * @return  Success or Failure
 */
static bStatus_t cbeaconProfile_WriteAttrCB( uint16 connHandle, gattAttribute_t *pAttr,
                                 uint8 *pValue, uint8 len, uint16 offset )
{
  bStatus_t status = SUCCESS;
  uint8 notifyApp = 0xFF;
  
  // If attribute permissions require authorization to write, return error
  if ( gattPermitAuthorWrite( pAttr->permissions ) )
  {
    // Insufficient authorization
    return ( ATT_ERR_INSUFFICIENT_AUTHOR );
  }
  
  if ( pAttr->type.len == ATT_BT_UUID_SIZE )
  {
    // 16-bit UUID
    uint16 uuid = BUILD_UINT16( pAttr->type.uuid[0], pAttr->type.uuid[1]);
    switch ( uuid )
    {
      case CBEACONPROFILE_CHAR1_UUID:
      case CBEACONPROFILE_CHAR3_UUID:

        //Validate the value
        // Make sure it's not a blob oper
        if ( offset == 0 )
        {
          if ( len != 1 )
          {
            status = ATT_ERR_INVALID_VALUE_SIZE;
          }
        }
        else
        {
          status = ATT_ERR_ATTR_NOT_LONG;
        }
        
        //Write the value
        if ( status == SUCCESS )
        {
          uint8 *pCurValue = (uint8 *)pAttr->pValue;        
          *pCurValue = pValue[0];

          if( pAttr->pValue == &cbeaconProfileChar1 )
          {
            notifyApp = CBEACONPROFILE_CHAR1;        
          }
          else
          {
            notifyApp = CBEACONPROFILE_CHAR3;           
          }
        }
             
        break;

      case GATT_CLIENT_CHAR_CFG_UUID:
        status = GATTServApp_ProcessCCCWriteReq( connHandle, pAttr, pValue, len,
                                                 offset, GATT_CLIENT_CFG_NOTIFY );
        break;
        
      default:
        // Should never get here! (characteristics 2 and 4 do not have write permissions)
        status = ATT_ERR_ATTR_NOT_FOUND;
        break;
    }
  }
  else
  {
    // 128-bit UUID
    status = ATT_ERR_INVALID_HANDLE;
  }

  // If a charactersitic value changed then callback function to notify application of change
  if ( (notifyApp != 0xFF ) && cbeaconProfile_AppCBs && cbeaconProfile_AppCBs->pfncBeaconProfileChange )
  {
    cbeaconProfile_AppCBs->pfncBeaconProfileChange( notifyApp );  
  }
  
  return ( status );
}

/*********************************************************************
 * @fn          cbeaconProfile_HandleConnStatusCB
 *
 * @brief       cBeacon Profile link status change handler function.
 *
 * @param       connHandle - connection handle
 * @param       changeType - type of change
 *
 * @return      none
 */
static void cbeaconProfile_HandleConnStatusCB( uint16 connHandle, uint8 changeType )
{ 
  // Make sure this is not loopback connection
  if ( connHandle != LOOPBACK_CONNHANDLE )
  {
    // Reset Client Char Config if connection has dropped
    if ( ( changeType == LINKDB_STATUS_UPDATE_REMOVED )      ||
         ( ( changeType == LINKDB_STATUS_UPDATE_STATEFLAGS ) && 
           ( !linkDB_Up( connHandle ) ) ) )
    { 
      GATTServApp_InitCharCfg( connHandle, cbeaconProfileChar4Config );
    }
  }
}


/*********************************************************************
*********************************************************************/
