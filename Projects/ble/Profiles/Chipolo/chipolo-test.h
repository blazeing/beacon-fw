/**************************************************************************************************
  Filename: chipolo-test.h
  Author: Blaz Remskar
  Date: 27.6.2015
  Revision: 0

  Description: Chipolo test service
**************************************************************************************************/

#ifndef CHIPOLOTEST_H
#define CHIPOLOTEST_H

#ifdef __cplusplus
extern "C"
{
#endif

/*********************************************************************
 * INCLUDES
 */

/*********************************************************************
 * CONSTANTS
 */

// Profile Parameters
#define CHIPOLO_ENABLER                 0  // RW uint8 - Profile Attribute value
#define CHIPOLO_1_ATTR                  1  // RW int16 - Profile Attribute value
#define CHIPOLO_2_ATTR                  2  // RW int16 - Profile Attribute value
#define CHIPOLO_3_ATTR                  3  // RW int16 - Profile Attribute value
#define CHIPOLO_RANGE                   4  // RW uint16 - Profile Attribute value
  
// Profile UUIDs
#define CHIPOLO_RANGE_UUID              0xABC1  
#define CHIPOLO_ENABLER_UUID            0xABC2
#define CHIPOLO_1_UUID                  0xABC3
#define CHIPOLO_2_UUID                  0xABC4
#define CHIPOLO_3_UUID                  0xABC5
  
// Chipolo Service UUID
#define CHIPOLO_SERVICE_UUID            0xABCD
  
// Profile Range Values
#define CHIPOLO_RANGE1                  0
#define CHIPOLO_RANGE2                  1

// Profile Services bit fields
#define CHIPOLO_SERVICE                 0x00000001

// Configuration NV Items - Range  0x80 ---- 
#define BLE_INTERVALS                   0x80
 
   
   
/*********************************************************************
 * TYPEDEFS
 */

/*********************************************************************
 * MACROS
 */

/*********************************************************************
 * Profile Callbacks
 */
// Callback when the device has been started.  Callback event to 
// the ask for a battery check.
typedef void (*chipoloEnabler_t)( void );
typedef void (*chipoloRange_t)( void );

typedef struct
{
  chipoloEnabler_t        pfnChipoloEnabler;  // Called when Enabler attribute changes
  chipoloRange_t        pfnChipoloRange;  // Called when Enabler attribute changes
} chipoloCBs_t;

/*********************************************************************
 * API FUNCTIONS 
 */

/*
 * Chipolo_AddService- Initializes the Chipolo service by registering 
 *          GATT attributes with the GATT server. Only call this function once.
 *
 * @param   services - services to add. This is a bit map and can
 *                     contain more than one service.
 */
extern bStatus_t Chipolo_AddService( uint32 services );

/*
 * Chipolo_RegisterAppCBs - Registers the application callback function.
 *                    Only call this function once.
 *
 *    appCallbacks - pointer to application callbacks.
 */
extern bStatus_t Chipolo_RegisterAppCBs( chipoloCBs_t *appCallbacks );


/*
 * Chipolo_SetParameter - Set an Chipolo Profile parameter.
 *
 *    param - Profile parameter ID
 *    len - length of data to right
 *    value - pointer to data to write.  This is dependent on
 *          the parameter ID and WILL be cast to the appropriate 
 *          data type (example: data type of uint16 will be cast to 
 *          uint16 pointer).
 */
extern bStatus_t Chipolo_SetParameter( uint8 param, uint8 len, void *value );
  
/*
 * Chipolo_GetParameter - Get an Chipolo Profile parameter.
 *
 *    param - Profile parameter ID
 *    value - pointer to data to write.  This is dependent on
 *          the parameter ID and WILL be cast to the appropriate 
 *          data type (example: data type of uint16 will be cast to 
 *          uint16 pointer).
 */
extern bStatus_t Chipolo_GetParameter( uint8 param, void *value );

void setChipolo1Parameter(void);

/*********************************************************************
*********************************************************************/

#ifdef __cplusplus
}
#endif

#endif /* CHIPOLOTEST_H */
