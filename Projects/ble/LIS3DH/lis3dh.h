#ifndef _LIS3DH_SENSOR_H_
#define _LIS3DH_SENSOR_H_

// LIS3 accelerometer register map  
#define LIS3_STATUS_REG_AUX             0x07
#define LIS3_OUT_ADC1_L                 0x08
#define LIS3_OUT_ADC1_H                 0x09
#define LIS3_OUT_ADC2_L                 0x0A
#define LIS3_OUT_ADC2_H                 0x0B
#define LIS3_OUT_ADC3_L                 0x0C
#define LIS3_OUT_ADC3_H                 0x0D
#define LIS3_INT_COUNTER_REG            0x0E
#define LIS3_WHO_AM_I                   0x0F
#define LIS3_TEMP_CFG_REG               0x1F
#define LIS3_CTRL_REG1                  0x20
#define LIS3_CTRL_REG2                  0x21
#define LIS3_CTRL_REG3                  0x22
#define LIS3_CTRL_REG4                  0x23
#define LIS3_CTRL_REG5                  0x24
#define LIS3_CTRL_REG6                  0x25
#define LIS3_REFERENCE                  0x26
#define LIS3_STATUS_REG                 0x27
#define LIS3_OUT_X_L                    0x28
#define LIS3_OUT_X_H                    0x29
#define LIS3_OUT_Y_L                    0x2A
#define LIS3_OUT_Y_H                    0x2B
#define LIS3_OUT_Z_L                    0x2C
#define LIS3_OUT_Z_H                    0x2D
#define LIS3_FIFO_CTRL_REG              0x2E
#define LIS3_FIFO_SRC_REG               0x2F
#define LIS3_INT1_CFG                   0x30
#define LIS3_INT1_SOURCE                0x31
#define LIS3_INT1_THS                   0x32
#define LIS3_INT1_DURATION              0x33
#define LIS3_CLICK_CFG                  0x38
#define LIS3_CLICK_SRC                  0x39
#define LIS3_CLICK_THS                  0x3A
#define LIS3_TIME_LIMIT                 0x3B
#define LIS3_TIME_LATENCY               0x3C
#define LIS3_TIME_WINDOW                0x3D

#endif






