/**************************************************************************************************
  Filename: cBeacon.c
  Author: Blaz Remskar
  Date: 5.14.2015
  Revision: 0

  Description: This file contains the cBeacon BLE peripheral application.

**************************************************************************************************/

/*********************************************************************
 * INCLUDES
 */

#include "bcomdef.h"
#include "OSAL.h"
#include "OSAL_PwrMgr.h"

#include "OnBoard.h"
#include "hal_adc.h"
#include "hal_led.h"
#include "hal_key.h"
#include "hal_lcd.h"

#include "gatt.h"

#include "hci.h"

#include "gapgattserver.h"
#include "gattservapp.h"
#include "devinfoservice.h"
#include "cbeaconGATTprofile.h"

#if defined( CC2540_MINIDK )
  #include "cbeaconkeys.h"
#endif

#include "peripheral.h"

#include "gapbondmgr.h"

#include "cBeacon.h"

#include "lis3dh.h"

#if defined FEATURE_OAD
  #include "oad.h"
  #include "oad_target.h"
#endif

/*********************************************************************
 * MACROS
 */

/*********************************************************************
 * CONSTANTS
 */

// How often to perform periodic event
#define CBE_PERIODIC_EVT_PERIOD                   5000

// What is the advertising interval when device is discoverable (units of 625us, 160=100ms)
#define DEFAULT_ADVERTISING_INTERVAL          160
#define FIVE_SEC_ADVERTISING_INTERVAL         8000
#define ONE_SEC_ADVERTISING_INTERVAL          1600

// Limited discoverable mode advertises for 30.72s, and then stops
// General discoverable mode advertises indefinitely

#if defined ( CC2540_MINIDK )
#define DEFAULT_DISCOVERABLE_MODE             GAP_ADTYPE_FLAGS_LIMITED
#else
#define DEFAULT_DISCOVERABLE_MODE             GAP_ADTYPE_FLAGS_GENERAL
#endif  // defined ( CC2540_MINIDK )

// Minimum connection interval (units of 1.25ms, 80=100ms) if automatic parameter update request is enabled
#define DEFAULT_DESIRED_MIN_CONN_INTERVAL     80

// Maximum connection interval (units of 1.25ms, 800=1000ms) if automatic parameter update request is enabled
#define DEFAULT_DESIRED_MAX_CONN_INTERVAL     800

// Slave latency to use if automatic parameter update request is enabled
#define DEFAULT_DESIRED_SLAVE_LATENCY         0

// Supervision timeout value (units of 10ms, 1000=10s) if automatic parameter update request is enabled
#define DEFAULT_DESIRED_CONN_TIMEOUT          1000

// Whether to enable automatic parameter update request when a connection is formed
#define DEFAULT_ENABLE_UPDATE_REQUEST         TRUE

// Connection Pause Peripheral time value (in seconds)
#define DEFAULT_CONN_PAUSE_PERIPHERAL         6

// Company Identifier: Texas Instruments Inc. (13)
#define TI_COMPANY_ID                         0x000D

#define INVALID_CONNHANDLE                    0xFFFF

// Length of bd addr as a string
#define B_ADDR_STR_LEN                        15

// Test service
#define CBEACON_SERVICE                       0xB00B

#define HW_VERSION__DEFINE                    4
#define OPTIONS_SHAKE_N_FIND                  0x04

/*********************************************************************
 * TYPEDEFS
 */

/*********************************************************************
 * GLOBAL VARIABLES
 */
bool AccINT1flag = false;
uint16 SNFConfigBuf = 10;

bool bLIS3DH_self_test_enabled = false;
bool bLIS3DH_self_test_OK = false;
bool CodeOK  = false;

uint8 ChipoloOptionsBuf;



/*********************************************************************
 * EXTERNAL VARIABLES
 */

/*********************************************************************
 * EXTERNAL FUNCTIONS
 */

/*********************************************************************
 * LOCAL VARIABLES
 */
static uint8 cBeaconBLE_TaskID;   // Task ID for internal task/event processing

static gaprole_States_t gapProfileState = GAPROLE_INIT;

// GAP - SCAN RSP data (max size = 31 bytes)
static uint8 scanRspData[] =
{
  // complete name
  0x08,   // length of this data
  GAP_ADTYPE_LOCAL_NAME_COMPLETE,
  0x63,   // 'c'
  0x42,   // 'B'
  0x65,   // 'e'
  0x61,   // 'a'
  0x63,   // 'c'
  0x6f,   // 'o'
  0x6e,   // 'n'

  // connection interval range
  0x05,   // length of this data
  GAP_ADTYPE_SLAVE_CONN_INTERVAL_RANGE,
  LO_UINT16( DEFAULT_DESIRED_MIN_CONN_INTERVAL ),   // 100ms
  HI_UINT16( DEFAULT_DESIRED_MIN_CONN_INTERVAL ),
  LO_UINT16( DEFAULT_DESIRED_MAX_CONN_INTERVAL ),   // 1s
  HI_UINT16( DEFAULT_DESIRED_MAX_CONN_INTERVAL ),

  // Tx power level
  0x02,   // length of this data
  GAP_ADTYPE_POWER_LEVEL,
  0x00,   // 0dBm

  0x0A,   //data length
  GAP_ADTYPE_SERVICE_DATA,
  LO_UINT16(CBEACON_SERVICE),HI_UINT16(CBEACON_SERVICE), //UUID PROFILE
  0x00,0x01,0x00,0x02,0x99,0x99,0x64 //CUSTOM DATA
};

// GAP - Advertisement data (max size = 31 bytes, though this is
// best kept short to conserve power while advertisting)
static uint8 advertDataAAAA[] =
{

  0x02, // length of this data
  GAP_ADTYPE_FLAGS,
  0x1A, //GAP_ADTYPE_FLAGS_GENERAL | GAP_ADTYPE_FLAGS_BREDR_NOT_SUPPORTED,

  0x1A, // length of this data (26 Bytes )
  0xFF,
  /*Apple Pre-Amble*/
  0x4c,
  0x00,
  0x02,
  0x15,
  //4f0358e0-2ee7-11e4-8c21-0800200c9a66 Nextome default uuid
  /*Device UUID (16 Bytes)*/
  0x4F, 0x03, 0x58, 0xE0, 0x2E, 0xE7, 0x11, 0xE4, 0x8C, 0x21, 0x08, 0x00, 0x20, 0x0C, 0x9A, 0x66,

  /*Major Value (2 Bytes)*/
  0x00, 0x01,

  /*Minor Value (2 Bytes)*/
  0x00, 0x02,

  /*Measured Power*/
  0xC6,
};

// GAP - Advertisement data (max size = 31 bytes, though this is
// best kept short to conserve power while advertisting)
static uint8 advertDataBBBB[] =
{

  0x02, // length of this data
  GAP_ADTYPE_FLAGS,
  0x1A, //GAP_ADTYPE_FLAGS_GENERAL | GAP_ADTYPE_FLAGS_BREDR_NOT_SUPPORTED,

  0x1A, // length of this data (26 Bytes )
  0xFF,
  /*Apple Pre-Amble*/
  0x4c,
  0x00,
  0x02,
  0x15,
  //4f0358e0-2ee7-11e4-8c21-0800200c9a66 Nextome default uuid
  /*Device UUID (16 Bytes)*/
  0x4F, 0x03, 0x58, 0xE0, 0x2E, 0xE7, 0x11, 0xE4, 0x8C, 0x21, 0x08, 0x00, 0x20, 0x0C, 0x9A, 0x67,

  /*Major Value (2 Bytes)*/
  0x00, 0x01,

  /*Minor Value (2 Bytes)*/
  0x00, 0x02,

  /*Measured Power*/
  0xC6,
};

// GAP GATT Attributes
static uint8 attDeviceName[GAP_DEVICE_NAME_LEN] = "cBeacon peripheral";

/*********************************************************************
 * LOCAL FUNCTIONS
 */
static void cBeaconBLE_ProcessOSALMsg( osal_event_hdr_t *pMsg );
static void cBeaconStateNotificationCB( gaprole_States_t newState );
static void performPeriodicTask( void );
static void cBeaconProfileChangeCB( uint8 paramID );

void AccelerometerInit(bool FreeFall);
void AccelerometerPower(bool power);

void SPIwrite(uint8 addr, uint8 data);
uint8 SPIread(uint8 addr);


#if defined( CC2540_MINIDK )
static void cBeaconBLE_HandleKeys( uint8 shift, uint8 keys );
#endif

#if (defined HAL_LCD) && (HAL_LCD == TRUE)
static char *bdAddr2Str ( uint8 *pAddr );
#endif // (defined HAL_LCD) && (HAL_LCD == TRUE)



/*********************************************************************
 * PROFILE CALLBACKS
 */

// GAP Role Callbacks
static gapRolesCBs_t cBeaconBLE_PeripheralCBs =
{
  cBeaconStateNotificationCB,  // Profile State Change Callbacks
  NULL                            // When a valid RSSI is read from controller (not used by application)
};

// GAP Bond Manager Callbacks
static gapBondCBs_t cBeaconBLE_BondMgrCBs =
{
  NULL,                     // Passcode callback (not used by application)
  NULL                      // Pairing / Bonding state Callback (not used by application)
};

// GATT Profile Callbacks
static cbeaconProfileCBs_t cBeaconBLE_profileCBs =
{
  cBeaconProfileChangeCB    // Charactersitic value change callback
};
/*********************************************************************
 * PUBLIC FUNCTIONS
 */

/*********************************************************************
 * @fn      BLEcBeacon_Init
 *
 * @brief   Initialization function for the BLE cBeacon App Task.
 *          This is called during initialization and should contain
 *          any application specific initialization (ie. hardware
 *          initialization/setup, table initialization, power up
 *          notificaiton ... ).
 *
 * @param   task_id - the ID assigned by OSAL.  This ID should be
 *                    used to send messages and set timers.
 *
 * @return  none
 */
void BLEcBeacon_Init( uint8 task_id )
{
  cBeaconBLE_TaskID = task_id;

  // Setup the GAP
  VOID GAP_SetParamValue( TGAP_CONN_PAUSE_PERIPHERAL, DEFAULT_CONN_PAUSE_PERIPHERAL );
  
  // Setup the GAP Peripheral Role Profile
  {
    #if defined( CC2540_MINIDK )
      // For the CC2540DK-MINI keyfob, device doesn't start advertising until button is pressed
      uint8 initial_advertising_enable = FALSE;
    #else
      // For other hardware platforms, device starts advertising upon initialization
      uint8 initial_advertising_enable = TRUE;
    #endif

    // By setting this to zero, the device will go into the waiting state after
    // being discoverable for 30.72 second, and will not being advertising again
    // until the enabler is set back to TRUE
    uint16 gapRole_AdvertOffTime = 0;

    uint8 enable_update_request = DEFAULT_ENABLE_UPDATE_REQUEST;
    uint16 desired_min_interval = DEFAULT_DESIRED_MIN_CONN_INTERVAL;
    uint16 desired_max_interval = DEFAULT_DESIRED_MAX_CONN_INTERVAL;
    uint16 desired_slave_latency = DEFAULT_DESIRED_SLAVE_LATENCY;
    uint16 desired_conn_timeout = DEFAULT_DESIRED_CONN_TIMEOUT;

    // Set the GAP Role Parameters
    GAPRole_SetParameter( GAPROLE_ADVERT_ENABLED, sizeof( uint8 ), &initial_advertising_enable );
    GAPRole_SetParameter( GAPROLE_ADVERT_OFF_TIME, sizeof( uint16 ), &gapRole_AdvertOffTime );

    GAPRole_SetParameter( GAPROLE_SCAN_RSP_DATA, sizeof ( scanRspData ), scanRspData );
    GAPRole_SetParameter( GAPROLE_ADVERT_DATA, sizeof( advertDataAAAA ), advertDataAAAA );

    
    GAPRole_SetParameter( GAPROLE_PARAM_UPDATE_ENABLE, sizeof( uint8 ), &enable_update_request );
    GAPRole_SetParameter( GAPROLE_MIN_CONN_INTERVAL, sizeof( uint16 ), &desired_min_interval );
    GAPRole_SetParameter( GAPROLE_MAX_CONN_INTERVAL, sizeof( uint16 ), &desired_max_interval );
    GAPRole_SetParameter( GAPROLE_SLAVE_LATENCY, sizeof( uint16 ), &desired_slave_latency );
    GAPRole_SetParameter( GAPROLE_TIMEOUT_MULTIPLIER, sizeof( uint16 ), &desired_conn_timeout );
  }

  // Set the GAP Characteristics
  GGS_SetParameter( GGS_DEVICE_NAME_ATT, GAP_DEVICE_NAME_LEN, attDeviceName );

  // Set advertising interval
  {
    uint16 advInt = ONE_SEC_ADVERTISING_INTERVAL;

    GAP_SetParamValue( TGAP_LIM_DISC_ADV_INT_MIN, advInt );
    GAP_SetParamValue( TGAP_LIM_DISC_ADV_INT_MAX, advInt );
    GAP_SetParamValue( TGAP_GEN_DISC_ADV_INT_MIN, advInt );
    GAP_SetParamValue( TGAP_GEN_DISC_ADV_INT_MAX, advInt );
  }

  // Setup the GAP Bond Manager
  {
    //uint32 passkey = 0; // passkey "000000"
    uint8 pairMode = GAPBOND_PAIRING_MODE_NO_PAIRING;
    uint8 mitm = FALSE;
    uint8 ioCap = GAPBOND_IO_CAP_NO_INPUT_NO_OUTPUT;
    uint8 bonding = FALSE;
    //GAPBondMgr_SetParameter( GAPBOND_DEFAULT_PASSCODE, sizeof ( uint32 ), &passkey );
    GAPBondMgr_SetParameter( GAPBOND_PAIRING_MODE, sizeof ( uint8 ), &pairMode );
    GAPBondMgr_SetParameter( GAPBOND_MITM_PROTECTION, sizeof ( uint8 ), &mitm );
    GAPBondMgr_SetParameter( GAPBOND_IO_CAPABILITIES, sizeof ( uint8 ), &ioCap );
    GAPBondMgr_SetParameter( GAPBOND_BONDING_ENABLED, sizeof ( uint8 ), &bonding );
  }

  // Initialize GATT attributes
  GGS_AddService( GATT_ALL_SERVICES );            // GAP
  GATTServApp_AddService( GATT_ALL_SERVICES );    // GATT attributes
  DevInfo_AddService();                           // Device Information Service
  cBeaconProfile_AddService( GATT_ALL_SERVICES );  // GATT Profile
#if defined FEATURE_OAD
  VOID OADTarget_AddService();                    // OAD Profile
#endif

  // Setup the cBeaconProfile Characteristic Values
  {
    uint8 charValue1 = 1;
    uint8 charValue2 = 2;
    uint8 charValue3 = 3;
    uint8 charValue4 = 4;
    uint8 charValue5[CBEACONPROFILE_CHAR5_LEN] = { 1, 2, 3, 4, 5 };
    cBeaconProfile_SetParameter( CBEACONPROFILE_CHAR1, sizeof ( uint8 ), &charValue1 );
    cBeaconProfile_SetParameter( CBEACONPROFILE_CHAR2, sizeof ( uint8 ), &charValue2 );
    cBeaconProfile_SetParameter( CBEACONPROFILE_CHAR3, sizeof ( uint8 ), &charValue3 );
    cBeaconProfile_SetParameter( CBEACONPROFILE_CHAR4, sizeof ( uint8 ), &charValue4 );
    cBeaconProfile_SetParameter( CBEACONPROFILE_CHAR5, CBEACONPROFILE_CHAR5_LEN, charValue5 );
  }


#if defined( CC2540_MINIDK )

  SK_AddService( GATT_ALL_SERVICES ); // Keys Profile

  // Register for all key events - This app will handle all key events
  RegisterForKeys( cBeaconBLE_TaskID );

  // makes sure LEDs are off
  HalLedSet( (HAL_LED_1 | HAL_LED_2), HAL_LED_MODE_OFF );

  // For keyfob board set GPIO pins into a power-optimized state
  // Note that there is still some leakage current from the buzzer,
  // accelerometer, LEDs, and buttons on the PCB.

  P0SEL = 0; // Configure Port 0 as GPIO
  P1SEL = 0; // Configure Port 1 as GPIO
  P2SEL = 0; // Configure Port 2 as GPIO

  P0DIR = 0xFC; // Port 0 pins P0.0 and P0.1 as input (buttons),
                // all others (P0.2-P0.7) as output
  P1DIR = 0xFF; // All port 1 pins (P1.0-P1.7) as output
  P2DIR = 0x1F; // All port 1 pins (P2.0-P2.4) as output

  P0 = 0x03; // All pins on port 0 to low except for P0.0 and P0.1 (buttons)
  P1 = 0;   // All pins on port 1 to low
  P2 = 0;   // All pins on port 2 to low

#endif // #if defined( CC2540_MINIDK )

 
////////////////////////////////////////////////////////////////////////////////
  
  PERCFG = 0x21; // TIMER1, TIMER4, UART1 on alternative 1 location, TIMER3, UART0 on alternative 2 location  
  
#if ( (HW_VERSION__DEFINE == 4) || (HW_VERSION__DEFINE == 3) )

  P0SEL = 0x70; // Configure P0.4, P0.5 as USART1-UART pins, P0.6 as Timer1 output, all other as GPIO

#elif ( (HW_VERSION__DEFINE == 2) || (HW_VERSION__DEFINE == 1) )
    
  P0SEL = 0x30; // Configure P0.4, P0.5 as USART1-UART pins, all other as GPIO

#endif
  

  P1SEL = 0x38; // Configure P1.3...5 as USART0-SPI pins, all other as GPIO
  P2SEL = 0;    // Configure Port 2 as GPIO, USART0 has priority over USART1

#if ( (HW_VERSION__DEFINE == 4) || (HW_VERSION__DEFINE == 3) )
  
  P0DIR = 0xDF; // P0.5 as input, all other as outputs
    
#elif ( HW_VERSION__DEFINE == 2 )
    
  P0DIR = 0x97; // P0.3, P0.5, P0.6 as inputs, all other as outputs
    
#elif ( HW_VERSION__DEFINE == 1 )
    
  P0DIR = 0xD3; // P0.2, P0.3, P0.5 as inputs, all other as outputs
    
#endif

  P1DIR = 0xEC; // P1.0, P1.1, P1.4 as inputs, all other as outputs
  P2DIR = 0x1F; // All port 2 pins (P2.0-P2.4) as outputs, 1st priority USART0, 2nd priority USART1, 3rd priority TIMER1
  
  P0INP = 0xFF; //all P0 inputs -> 3-state
  P1INP = 0xFE; //P1.0 -> pull-up/down, all other P1 inputs -> 3-state
  P2INP = 0xFF; //all P2 inputs -> 3-state (P0, P1, P2 pull-up/down -> pull-down)
  
  P0 = 0;     // All pins on port 0 to low
  P1 = 0x0C;  // P1.2, P1.3 to high, all others to low
  P2 = 0;     // All pins on port 2 to low
      
  
// Set-up PWM Timer  
#if ( (HW_VERSION__DEFINE == 4) || (HW_VERSION__DEFINE == 3) )

  T1CTL = 0x00;       //Prescaler = 1, timer disabled
  T1CNTL = 0;         //clear counter, initialize output pins
  T1CCTL4 = 0x14;     //toggle output on compare, compare mode

#endif
          
  // Set-up SampleRate Timer
  T3CTL = 0xEE;   //Prescaler = 128, stop timer, enable IRQ, clear counter and init pins, Timer mode = modulo from 0x00 fo T3CC0
  T3CC0 = 250;    //SampleRate = 32MHz / 128 / 250 = 1kHz
  T3CCTL0 = 0x44; //channel 0; enable IRQ, set output on compare, compare mode, no capture
  T3CCTL1 = 0;    //channel 1; disable IRQ, set output on compare, capture mode, no capture  
  T3IE = 1;       //enable timer3 interrupts
  
  //Setup ADC
  HalAdcSetReference(HAL_ADC_REF_125V);
  
  //setup USART0-SPI
  U0UCR  = 0x80;        // Flush and goto IDLE state. 8-N-1
  U0CSR  = 0x00;        // SPI mode, master
  U0GCR  = 0x20 | 15;   // Negative clock polarity, clock phase = 0, MSB first, baud exponent = 15
  U0BAUD = 0;           // BaudRate = 1MHz ( ((256 + baud_m) * 2^baud_exponent) / 2^28 * 32MHz )
  U0CSR |= 0x40;        // Enable receiver
               
  //setup USART1-UART
  U1UCR  = 0x82;        // Flush and goto IDLE state. 8-N-1
  U1CSR  = 0x80;        // UART mode, master
  U1GCR  = 0x00 | 8;    // Negative clock polarity, clock phase = 0, LSB first, baud exponent = 11
  U1BAUD = 59;          // BaudRate = 9600bps ( ((256 + baud_m) * 2^baud_exponent) / 2^28 * 32MHz )
  IEN0 |= 0x08;         // Enable USART1 Rx interrupt
  //IEN2 |= 0x08;         // Enable USART1 Tx interrupt
  U1CSR |= 0x40;        // Enable receiver
  

  //Initialize accelerometer
  AccelerometerInit(FALSE);
  AccelerometerPower(TRUE);
      
  //configure pin interrupt on P1.0
  PICTL = 0x00; //minimum drive strength enhancement, P0, P1, P2 rising edge interrupt
  P1IEN = 0x01; //P1.0 interrupt enabled
  IEN2 |= 0x10; //enable Port1 interrupts
  P1IFG = 0x00; //clear all pin interrupt status flags
  P1IF = 0;     //clear Port1 interrupt flag
  
////////////////////////////////////////////////////////////////////////////////  
  
  
#if (defined HAL_LCD) && (HAL_LCD == TRUE)

#if defined FEATURE_OAD
  #if defined (HAL_IMAGE_A)
    HalLcdWriteStringValue( "BLE Peri-A", OAD_VER_NUM( _imgHdr.ver ), 16, HAL_LCD_LINE_1 );
  #else
    HalLcdWriteStringValue( "BLE Peri-B", OAD_VER_NUM( _imgHdr.ver ), 16, HAL_LCD_LINE_1 );
  #endif // HAL_IMAGE_A
#else
  HalLcdWriteString( "BLE Peripheral", HAL_LCD_LINE_1 );
#endif // FEATURE_OAD

#endif // (defined HAL_LCD) && (HAL_LCD == TRUE)

  // Register callback with GATT profile
  VOID cBeaconProfile_RegisterAppCBs( &cBeaconBLE_profileCBs );

  // Enable clock divide on halt
  // This reduces active current while radio is active and CC254x MCU
  // is halted
  HCI_EXT_ClkDivOnHaltCmd( HCI_EXT_ENABLE_CLK_DIVIDE_ON_HALT );

#if defined ( DC_DC_P0_7 )

  // Enable stack to toggle bypass control on TPS62730 (DC/DC converter)
  HCI_EXT_MapPmIoPortCmd( HCI_EXT_PM_IO_PORT_P0, HCI_EXT_PM_IO_PORT_PIN7 );

#endif // defined ( DC_DC_P0_7 )

  // Setup a delayed profile startup
  osal_set_event( cBeaconBLE_TaskID, CBE_START_DEVICE_EVT );

}

/*********************************************************************
 * @fn      BLEcBeacon_ProcessEvent
 *
 * @brief   BLE cBeacon application Task event processor.  This function
 *          is called to process all events for the task.  Events
 *          include timers, messages and any other user defined events.
 *
 * @param   task_id  - The OSAL assigned task ID.
 * @param   events - events to process.  This is a bit map and can
 *                   contain more than one event.
 *
 * @return  events not processed
 */
uint16 BLEcBeacon_ProcessEvent( uint8 task_id, uint16 events )
{

  VOID task_id; // OSAL required parameter that isn't used in this function

  if ( events & SYS_EVENT_MSG )
  {
    uint8 *pMsg;

    if ( (pMsg = osal_msg_receive( cBeaconBLE_TaskID )) != NULL )
    {
      cBeaconBLE_ProcessOSALMsg( (osal_event_hdr_t *)pMsg );

      // Release the OSAL message
      VOID osal_msg_deallocate( pMsg );
    }

    // return unprocessed events
    return (events ^ SYS_EVENT_MSG);
  }

  if ( events & CBE_START_DEVICE_EVT )
  {
    // Start the Device
    VOID GAPRole_StartDevice( &cBeaconBLE_PeripheralCBs );

    // Start Bond Manager
    VOID GAPBondMgr_Register( &cBeaconBLE_BondMgrCBs );

    // Set timer for first periodic event
    osal_start_timerEx( cBeaconBLE_TaskID, CBE_PERIODIC_EVT, CBE_PERIODIC_EVT_PERIOD );

    return ( events ^ CBE_START_DEVICE_EVT );
  }

  if ( events & CBE_PERIODIC_EVT )
  {
    // Restart timer
    if ( CBE_PERIODIC_EVT_PERIOD )
    {
      osal_start_timerEx( cBeaconBLE_TaskID, CBE_PERIODIC_EVT, CBE_PERIODIC_EVT_PERIOD );
    }

    // Perform periodic application task
    performPeriodicTask();

    return (events ^ CBE_PERIODIC_EVT);
  }
  
  if ( events &  CBE_TIMEOUT_EVT )
  {
    // Change advert data
    GAPRole_SetParameter( GAPROLE_ADVERT_DATA, sizeof( advertDataAAAA ), advertDataAAAA );
    
    // Set advertising interval
    {
      uint16 advInt = ONE_SEC_ADVERTISING_INTERVAL;

      GAP_SetParamValue( TGAP_LIM_DISC_ADV_INT_MIN, advInt );
      GAP_SetParamValue( TGAP_LIM_DISC_ADV_INT_MAX, advInt );
      GAP_SetParamValue( TGAP_GEN_DISC_ADV_INT_MIN, advInt );
      GAP_SetParamValue( TGAP_GEN_DISC_ADV_INT_MAX, advInt );
    }

    return (events ^ CBE_TIMEOUT_EVT);
  }

  // Discard unknown events
  return 0;
}

/*********************************************************************
 * @fn      cBeaconBLE_ProcessOSALMsg
 *
 * @brief   Process an incoming task message.
 *
 * @param   pMsg - message to process
 *
 * @return  none
 */
static void cBeaconBLE_ProcessOSALMsg( osal_event_hdr_t *pMsg )
{
  switch ( pMsg->event )
  {
  #if defined( CC2540_MINIDK )
    case KEY_CHANGE:
      cBeaconBLE_HandleKeys( ((keyChange_t *)pMsg)->state, ((keyChange_t *)pMsg)->keys );
      break;
  #endif // #if defined( CC2540_MINIDK )

  default:
    // do nothing
    break;
  }
}

#if defined( CC2540_MINIDK )
/*********************************************************************
 * @fn      cBeaconBLE_HandleKeys
 *
 * @brief   Handles all key events for this device.
 *
 * @param   shift - true if in shift/alt.
 * @param   keys - bit field for key events. Valid entries:
 *                 HAL_KEY_SW_2
 *                 HAL_KEY_SW_1
 *
 * @return  none
 */
static void cBeaconBLE_HandleKeys( uint8 shift, uint8 keys )
{
  uint8 SK_Keys = 0;

  VOID shift;  // Intentionally unreferenced parameter

  if ( keys & HAL_KEY_SW_1 )
  {
    SK_Keys |= SK_KEY_LEFT;
  }

  if ( keys & HAL_KEY_SW_2 )
  {

    SK_Keys |= SK_KEY_RIGHT;

    // if device is not in a connection, pressing the right key should toggle
    // advertising on and off
    // Note:  If PLUS_BROADCASTER is define this condition is ignored and
    //        Device may advertise during connections as well. 
#ifndef PLUS_BROADCASTER  
    if( gapProfileState != GAPROLE_CONNECTED )
    {
#endif // PLUS_BROADCASTER
      uint8 current_adv_enabled_status;
      uint8 new_adv_enabled_status;

      //Find the current GAP advertisement status
      GAPRole_GetParameter( GAPROLE_ADVERT_ENABLED, &current_adv_enabled_status );

      if( current_adv_enabled_status == FALSE )
      {
        new_adv_enabled_status = TRUE;
      }
      else
      {
        new_adv_enabled_status = FALSE;
      }

      //change the GAP advertisement status to opposite of current status
      GAPRole_SetParameter( GAPROLE_ADVERT_ENABLED, sizeof( uint8 ), &new_adv_enabled_status );
#ifndef PLUS_BROADCASTER
    }
#endif // PLUS_BROADCASTER
  }

  // Set the value of the keys state to the Keys Profile;
  // This will send out a notification of the keys state if enabled
  SK_SetParameter( SK_KEY_ATTR, sizeof ( uint8 ), &SK_Keys );
}
#endif // #if defined( CC2540_MINIDK )

/*********************************************************************
 * @fn      cBeaconStateNotificationCB
 *
 * @brief   Notification from the profile of a state change.
 *
 * @param   newState - new state
 *
 * @return  none
 */
static void cBeaconStateNotificationCB( gaprole_States_t newState )
{
#ifdef PLUS_BROADCASTER
  static uint8 first_conn_flag = 0;
#endif // PLUS_BROADCASTER
  
  
  switch ( newState )
  {
    case GAPROLE_STARTED:
      {
        uint8 ownAddress[B_ADDR_LEN];
        uint8 systemId[DEVINFO_SYSTEM_ID_LEN];

        GAPRole_GetParameter(GAPROLE_BD_ADDR, ownAddress);

        // use 6 bytes of device address for 8 bytes of system ID value
        systemId[0] = ownAddress[0];
        systemId[1] = ownAddress[1];
        systemId[2] = ownAddress[2];

        // set middle bytes to zero
        systemId[4] = 0x00;
        systemId[3] = 0x00;

        // shift three bytes up
        systemId[7] = ownAddress[5];
        systemId[6] = ownAddress[4];
        systemId[5] = ownAddress[3];

        DevInfo_SetParameter(DEVINFO_SYSTEM_ID, DEVINFO_SYSTEM_ID_LEN, systemId);

        #if (defined HAL_LCD) && (HAL_LCD == TRUE)
          // Display device address
          HalLcdWriteString( bdAddr2Str( ownAddress ),  HAL_LCD_LINE_2 );
          HalLcdWriteString( "Initialized",  HAL_LCD_LINE_3 );
        #endif // (defined HAL_LCD) && (HAL_LCD == TRUE)
      }
      break;

    case GAPROLE_ADVERTISING:
      {
        #if (defined HAL_LCD) && (HAL_LCD == TRUE)
          HalLcdWriteString( "Advertising",  HAL_LCD_LINE_3 );
        #endif // (defined HAL_LCD) && (HAL_LCD == TRUE)
      }
      break;

    case GAPROLE_CONNECTED:
      {        
        #if (defined HAL_LCD) && (HAL_LCD == TRUE)
          HalLcdWriteString( "Connected",  HAL_LCD_LINE_3 );
        #endif // (defined HAL_LCD) && (HAL_LCD == TRUE)
          
#ifdef PLUS_BROADCASTER
        // Only turn advertising on for this state when we first connect
        // otherwise, when we go from connected_advertising back to this state
        // we will be turning advertising back on.
        if ( first_conn_flag == 0 ) 
        {
          uint8 adv_enabled_status = 1;
          GAPRole_SetParameter(GAPROLE_ADVERT_ENABLED, sizeof(uint8), &adv_enabled_status); // Turn on Advertising
          first_conn_flag = 1;
        }
#endif // PLUS_BROADCASTER
      }
      break;

    case GAPROLE_CONNECTED_ADV:
      {
        #if (defined HAL_LCD) && (HAL_LCD == TRUE)
          HalLcdWriteString( "Connected Advertising",  HAL_LCD_LINE_3 );
        #endif // (defined HAL_LCD) && (HAL_LCD == TRUE)
      }
      break;      
    case GAPROLE_WAITING:
      {
        #if (defined HAL_LCD) && (HAL_LCD == TRUE)
          HalLcdWriteString( "Disconnected",  HAL_LCD_LINE_3 );
        #endif // (defined HAL_LCD) && (HAL_LCD == TRUE)
      }
      break;

    case GAPROLE_WAITING_AFTER_TIMEOUT:
      {
        #if (defined HAL_LCD) && (HAL_LCD == TRUE)
          HalLcdWriteString( "Timed Out",  HAL_LCD_LINE_3 );
        #endif // (defined HAL_LCD) && (HAL_LCD == TRUE)
          
#ifdef PLUS_BROADCASTER
        // Reset flag for next connection.
        first_conn_flag = 0;
#endif //#ifdef (PLUS_BROADCASTER)
      }
      break;

    case GAPROLE_ERROR:
      {
        #if (defined HAL_LCD) && (HAL_LCD == TRUE)
          HalLcdWriteString( "Error",  HAL_LCD_LINE_3 );
        #endif // (defined HAL_LCD) && (HAL_LCD == TRUE)
      }
      break;

    default:
      {
        #if (defined HAL_LCD) && (HAL_LCD == TRUE)
          HalLcdWriteString( "",  HAL_LCD_LINE_3 );
        #endif // (defined HAL_LCD) && (HAL_LCD == TRUE)
      }
      break;

  }

  gapProfileState = newState;

#if !defined( CC2540_MINIDK )
  VOID gapProfileState;     // added to prevent compiler warning with
                            // "CC2540 Slave" configurations
#endif


}

/*********************************************************************
 * @fn      performPeriodicTask
 *
 * @brief   Perform a periodic application task. This function gets
 *          called every five seconds as a result of the CBE_PERIODIC_EVT
 *          OSAL event. In this example, the value of the third
 *          characteristic in the GATT profile service is retrieved
 *          from the profile, and then copied into the value of the
 *          the fourth characteristic.
 *
 * @param   none
 *
 * @return  none
 */
static void performPeriodicTask( void )
{
  uint8 valueToCopy;
  uint8 stat;

  // Call to retrieve the value of the third characteristic in the profile
  stat = cBeaconProfile_GetParameter( CBEACONPROFILE_CHAR3, &valueToCopy);

  if( stat == SUCCESS )
  {
    /*
     * Call to set that value of the fourth characteristic in the profile. Note
     * that if notifications of the fourth characteristic have been enabled by
     * a GATT client device, then a notification will be sent every time this
     * function is called.
     */
    cBeaconProfile_SetParameter( CBEACONPROFILE_CHAR4, sizeof(uint8), &valueToCopy);
  }
}

/*********************************************************************
 * @fn      cBeaconProfileChangeCB
 *
 * @brief   Callback from cBeaconBLEProfile indicating a value change
 *
 * @param   paramID - parameter ID of the value that was changed.
 *
 * @return  none
 */
static void cBeaconProfileChangeCB( uint8 paramID )
{
  uint8 newValue;

  switch( paramID )
  {
    case CBEACONPROFILE_CHAR1:
      cBeaconProfile_GetParameter( CBEACONPROFILE_CHAR1, &newValue );

      #if (defined HAL_LCD) && (HAL_LCD == TRUE)
        HalLcdWriteStringValue( "Char 1:", (uint16)(newValue), 10,  HAL_LCD_LINE_3 );
      #endif // (defined HAL_LCD) && (HAL_LCD == TRUE)

      break;

    case CBEACONPROFILE_CHAR3:
      cBeaconProfile_GetParameter( CBEACONPROFILE_CHAR3, &newValue );

      #if (defined HAL_LCD) && (HAL_LCD == TRUE)
        HalLcdWriteStringValue( "Char 3:", (uint16)(newValue), 10,  HAL_LCD_LINE_3 );
      #endif // (defined HAL_LCD) && (HAL_LCD == TRUE)

      break;

    default:
      // should not reach here!
      break;
  }
}

#if (defined HAL_LCD) && (HAL_LCD == TRUE)
/*********************************************************************
 * @fn      bdAddr2Str
 *
 * @brief   Convert Bluetooth address to string. Only needed when
 *          LCD display is used.
 *
 * @return  none
 */
char *bdAddr2Str( uint8 *pAddr )
{
  uint8       i;
  char        hex[] = "0123456789ABCDEF";
  static char str[B_ADDR_STR_LEN];
  char        *pStr = str;

  *pStr++ = '0';
  *pStr++ = 'x';

  // Start from end of addr
  pAddr += B_ADDR_LEN;

  for ( i = B_ADDR_LEN; i > 0; i-- )
  {
    *pStr++ = hex[*--pAddr >> 4];
    *pStr++ = hex[*pAddr & 0x0F];
  }

  *pStr = 0;

  return str;
}
#endif // (defined HAL_LCD) && (HAL_LCD == TRUE)

/*********************************************************************
*********************************************************************/

void AccelerometerInit(bool FreeFall)
{
  //Setup accelerometer
  SPIwrite(LIS3_TEMP_CFG_REG, 0xC0);    //enable ADC, enable Temperature sensor
  SPIwrite(LIS3_CTRL_REG1, 0x0F);       //data rate: power-down, low-power mode, enable X,Y,Z axis
  SPIwrite(LIS3_CTRL_REG3, 0x40);       //Interrupt generator 1 on INT1 enabled
  SPIwrite(LIS3_CTRL_REG4, 0x90);       //block data update, LSB @ lower address, scale: +-4g, high-resolution output disabled, self-test disabled, 4-wire SPI
  SPIwrite(LIS3_CTRL_REG5, 0x00);       //boot: normal mode, FIFO disabled, interrupt request not latched, 4D detection disabled
  SPIwrite(LIS3_CTRL_REG6, 0x00);       //INT2 interrupt disabled, interrupt active high  
  SPIwrite(LIS3_INT1_THS, 100); //Full scale = +-4g -> LSB = 31mg -> treshold = 31mg * SNFConfigBuf[0]
  
  if (FreeFall == true) {
    SPIwrite(LIS3_CTRL_REG2, 0x00);       //HPF disabled
    SPIwrite(LIS3_INT1_CFG, 0x95);        //enable interrupt generation on X,Y,Z low events, AND combination of interrupt events
    SPIwrite(LIS3_INT1_DURATION, (uint8)((SNFConfigBuf >> 8) & 0x00FF));      //ODR = 10Hz -> duration = SNFConfigBuf[1]/10Hz
  }
  else {
    SPIwrite(LIS3_CTRL_REG2, 0xCF);       //HPF autoreset on interrupt, HPF enabled on data, click, interrupt1 and interrupt2
    SPIwrite(LIS3_INT1_CFG, 0x2A);        //enable interrupt generation on X,Y,Z high events, OR combination of interrupt events
    //SPIwrite(LIS3_INT1_CFG, 0xAA);        //enable interrupt generation on X,Y,Z high events, AND combination of interrupt events
    SPIwrite(LIS3_INT1_DURATION, 3);      //ODR = 10Hz -> duration = SNFConfigBuf[1]/10Hz
  }
}

void AccelerometerPower(bool power)
{
  if (power) {
    SPIwrite(LIS3_CTRL_REG1, 0x2F);       //data rate: 10Hz low-power mode, enable X,Y,Z axis
  }
  else {    
    SPIwrite(LIS3_CTRL_REG1, 0x0F);       //data rate: power-down, low-power mode, enable X,Y,Z axis
  }
}

void SPIwrite(uint8 addr, uint8 data)
{
  ASSERT_CS();
  asm("NOP");
  asm("NOP");
  asm("NOP");
  asm("NOP");
  //clear flags
  U0CSR &= ~(0x1F);
  //send address
  U0DBUF = addr & 0x3F;
  //wait for transmit ok
  while(!(U0CSR & 0x02));
  //clear flags
  U0CSR &= ~(0x1F);
  //send data
  U0DBUF = data;
  //wait for transmit ok
  while(!(U0CSR & 0x02));
  asm("NOP");
  asm("NOP");
  asm("NOP");
  asm("NOP");
  RELEASE_CS();
}

uint8 SPIread(uint8 addr)
{
  uint8 data;
  
  ASSERT_CS();
  asm("NOP");
  asm("NOP");
  asm("NOP");
  asm("NOP");  
  //clear flags
  U0CSR &= ~(0x1F);
  //send address
  U0DBUF = 0x80 | (addr & 0x3F);
  //wait for transmit ok
  while(!(U0CSR & 0x02));
  //clear flags
  U0CSR &= ~(0x1F);
  //send data
  U0DBUF = 0;
  //wait for transmit ok
  while(!(U0CSR & 0x02));
  //read data
  data = U0DBUF;
  asm("NOP");
  asm("NOP");
  asm("NOP");
  asm("NOP");
  RELEASE_CS();
  
  return data;
}

HAL_ISR_FUNCTION( halP1IntIsr, P1INT_VECTOR )
{
  // Change advert data
  GAPRole_SetParameter( GAPROLE_ADVERT_DATA, sizeof( advertDataBBBB ), advertDataBBBB );
  
    // Set advertising interval
  {
    uint16 advInt = DEFAULT_ADVERTISING_INTERVAL;

    GAP_SetParamValue( TGAP_LIM_DISC_ADV_INT_MIN, advInt );
    GAP_SetParamValue( TGAP_LIM_DISC_ADV_INT_MAX, advInt );
    GAP_SetParamValue( TGAP_GEN_DISC_ADV_INT_MIN, advInt );
    GAP_SetParamValue( TGAP_GEN_DISC_ADV_INT_MAX, advInt );
  }
  
  // Set timer event for changing advert data
  osal_start_timerEx( cBeaconBLE_TaskID, CBE_TIMEOUT_EVT, 1000 );

  HAL_ENTER_ISR();
    
  /* P1.0 input interrupt */
  if (P1IFG & 0x01)
  {
    P1IFG &= ~(0x01);  /* Clear Interrupt Flag */
   
    //set Accelerometer INT1 flag
    AccINT1flag = true;
    
    //set LIS3DH self test OK flag
    bLIS3DH_self_test_OK = true;  
  }
  
  P1IFG = 0;
  P1IF = 0;
  
  HAL_EXIT_ISR();
}